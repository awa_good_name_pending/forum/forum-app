import { combineReducers } from 'redux';
import { authReducer } from './authReducer';
import { groupReducer } from './groupReducer';
import { uiReducer } from './uiReducer';

export const rootReducer = combineReducers({
  ui: uiReducer,
  auth: authReducer,
  group: groupReducer,
});
