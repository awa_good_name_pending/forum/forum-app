import Swal from 'sweetalert2';
import { withoutToken, withToken } from '../helpers/fetch';
import { types } from '../types/types';

export const startLogin = (email, password) => {
  return async (dispatch) => {
    try {
      const response = await withoutToken('auth', { email, password }, 'POST');
      const data = await response.json();

      if (data.ok) {
        localStorage.setItem('token', data.token);
        localStorage.setItem('init-token', new Date().getTime());

        dispatch(login({ uid: data.uid, name: data.name }));
      } else {
        Swal.fire('Error', data.msg, 'error');
      }
    } catch (error) {
      console.log(error);
    }
  };
};

const login = (user) => ({
  type: types.authLogin,
  payload: user,
});

export const startRegister = (name, email, password) => {
  return async (dispatch) => {
    try {
      const response = await withoutToken(
        'auth/new',
        { name, email, password },
        'POST'
      );
      const data = await response.json();
      if (data.ok) {
        dispatch(login({ uid: data.uid, name: data.name }));
      } else {
        Swal.fire('Error', data.msg, 'error');
      }
    } catch (error) {
      console.log(error);
    }
  };
};

export const startChecking = () => {
  return async (dispatch) => {
    try {
      const response = await withToken('auth/renew');
      const data = await response.json();
      if (data.ok) {
        dispatch(
          login({
            uid: data.uid,
            name: data.name,
          })
        );
      } else {
        dispatch(checkingFinish());
      }
    } catch (error) {
      console.log(error);
    }
  };
};

export const startLogout = () => {
  return (dispatch) => {
    localStorage.clear();
    dispatch(logout());
  };
};

const logout = () => ({
  type: types.authLogout,
});

const checkingFinish = () => ({ type: types.authCheckingFinish });
