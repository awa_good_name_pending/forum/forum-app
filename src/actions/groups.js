import { withToken } from '../helpers/fetch';
import { types } from '../types/types';

export const groupStartLoading = () => {
  return async (dispatch) => {
    try {
      const response = await withToken('groups');
      const data = await response.json();

      dispatch(groupsLoaded(data.groups));
    } catch (error) {
      console.log(error);
    }
  };
};

const groupsLoaded = (groups) => ({
  type: types.groupLoader,
  payload: groups,
});

export const startNewGroup = (name) => {
  return async (dispatch) => {
    try {
      const response = await withToken('groups', { name }, 'POST');
      const data = await response.json();
      if (data.ok) {
        dispatch(updateGroups(data.group));
      }
    } catch (error) {
      console.log(error);
    }
  };
};

const updateGroups = (group) => ({
  type: types.groupAddNew,
  payload: group,
});
