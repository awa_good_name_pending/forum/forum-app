import React from 'react';
import { Bar } from 'react-chartjs-2';
import Widgets from '../components/ui/Widgets';

const AnalyticsReds = () => {
  return (
    <div className='app'>
      <Bar
        data={{
          labels: ['Google', 'Facebook', 'Normal'],
          datasets: [
            {
              label: '# Personas que inician sesion por diferentes redes',
              data: [182, 120, 225],
              backgroundColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
              ],
              borderColor: [
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
              ],
              borderWidth: 3,
            },
          ],
        }}
        height={1100}
        width={1500}
        options={{
          maintainAspectRatio: false,
        }}
      />
      <Widgets />
    </div>
  );
};

export default AnalyticsReds;
