import React from 'react';
import { Bar } from 'react-chartjs-2';
import Widgets from '../components/ui/Widgets';

const Analytics = () => {
  return (
    <div className='app'>
      <Bar
        data={{
          labels: [
            'Cochabamba',
            'Santa Cruz',
            'La Paz',
            'Oruro',
            'Potosi',
            'Chuquisaca',
            'Beni',
            'Pando',
            'Tarija',
          ],
          datasets: [
            {
              label: '# Usuarios por departamento',
              data: [90, 70, 73, 50, 25, 34, 67, 43, 75],
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
              ],
              borderWidth: 3,
            },
          ],
        }}
        height={1100}
        width={1500}
        options={{
          maintainAspectRatio: false,
        }}
      />
      <Widgets />
    </div>
  );
};

export default Analytics;
