import React from 'react';
import { Pie, Bar } from 'react-chartjs-2';
import Widgets from '../components/ui/Widgets';

const AnalyticsSession = () => {
  return (
    <div className='app'>
      <Pie
        data={{
          labels: ['Hombres', 'Mujeres'],
          datasets: [
            {
              label: '# Usuarios por departamento',
              data: [290, 237],
              backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 99, 132, 0.2)',
              ],
              borderColor: ['rgba(54, 162, 235, 1)', 'rgba(255, 99, 132, 1)'],
              borderWidth: 3,
            },
          ],
        }}
        options={{
          maintainAspectRatio: false,
        }}
      />
      <Bar
        data={{
          labels: [
            'Cochabamba',
            'Santa Cruz',
            'La Paz',
            'Oruro',
            'Potosi',
            'Chuquisaca',
            'Beni',
            'Pando',
            'Tarija',
          ],
          datasets: [
            {
              label: '# Hombres por departamento',
              data: [53, 25, 43, 30, 17, 9, 44, 18, 52],
              backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
              ],
              borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
              ],
              borderWidth: 3,
            },
            {
              label: '# Mujeres por departamento',
              data: [37, 45, 30, 20, 8, 25, 23, 25, 23],
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(255, 99, 132, 1)',
              ],
              borderWidth: 3,
            },
          ],
        }}
        height={1100}
        width={1500}
        options={{
          maintainAspectRatio: false,
        }}
      />
      <Widgets />
    </div>
  );
};

export default AnalyticsSession;
