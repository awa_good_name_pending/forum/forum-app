import React from 'react';
import { Bar } from 'react-chartjs-2';
import Widgets from '../components/ui/Widgets';

const AnalyticsTime = () => {
  return (
    <div className='app'>
      <Bar
        data={{
          labels: [
            '0 - 00:30 min',
            '00:30 min - 01:00 hora',
            '01:00 hora - 01:30 horas',
            '01:30 horas - 02:00 horas',
            'Mas de 02:00 Horas',
          ],
          datasets: [
            {
              label: '# Usuarios por tiempo de actividad',
              data: [35, 45, 37, 30, 28],
              backgroundColor: [
                'rgba(244, 170, 170)',
                'rgba(229, 176, 255)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(177, 255, 155)',
                'rgba(180, 248, 255)',
                'rgba(255, 159, 64, 0.2)',
              ],
              borderColor: [
                'rgba(244, 6, 6 )',
                'rgba(170, 0, 255)',
                'rgba(255, 206, 86, 1)',
                'rgba(54, 255, 0)',
                'rgba(0, 232, 255 )',
                'rgba(255, 159, 64, 1)',
              ],
              borderWidth: 3,
            },
          ],
        }}
        height={1100}
        width={1500}
        options={{
          maintainAspectRatio: false,
        }}
      />
      <Widgets />
    </div>
  );
};

export default AnalyticsTime;
