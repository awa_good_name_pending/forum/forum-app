import React from 'react';
import { Link } from 'react-router-dom';
import SidebarOption from './SidebarOption';
import MailOutlineIcon from '@material-ui/icons/MailOutline';

const Widgets = () => {
  return (
    <div className='widgets'>
      <div className='widgets__widgetContainer'>
        <h2>DATA ANALYTICS</h2>
        <Link to='./home'>Atras</Link>
        <SidebarOption
          Icon={MailOutlineIcon}
          text='Ubicacion de los usuarios'
          path='/analyticsCountry'
        />
        <SidebarOption
          Icon={MailOutlineIcon}
          text='Usuarios por departamento'
          path='/analytics'
        />
        <SidebarOption
          Icon={MailOutlineIcon}
          text='Usuarios segun sexo'
          path='/analyticsSession'
        />
        <SidebarOption
          Icon={MailOutlineIcon}
          text='Usuarios activos'
          path='/analyticsGrup'
        />
        <SidebarOption
          Icon={MailOutlineIcon}
          text='Usuarios segun inicio de sesion'
          path='/analyticsReds'
        />
        <SidebarOption
          Icon={MailOutlineIcon}
          text='Usuarios tiempo por sesion'
          path='/analyticsTime'
        />
        <SidebarOption
          Icon={MailOutlineIcon}
          text='Usuarios por edad'
          path='/analyticsEdad'
        />
        <SidebarOption
          Icon={MailOutlineIcon}
          text='Interacciones en la app'
          path='/analyticsPost'
        />
      </div>
    </div>
  );
};

export default Widgets;
