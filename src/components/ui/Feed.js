import React, { useState } from 'react';
import TweetBox from './TweetBox';
import Post from './Post';
import FlipMove from 'react-flip-move';

const initialState = [
  {
    text: 'Mi primer post en el foro general',
    displayName: 'Diego',
    username: 'diego123',
    verified: true,
    avatar: 'https://wallpaperaccess.com/full/2213424.jpg',
  },
  {
    text: 'Post en el grupo general',
    displayName: 'Diego',
    username: 'ariel32',
    verified: true,
    avatar: 'https://wallpaperaccess.com/full/2213424.jpg',
  },
  {
    text: 'Prueba del post en general',
    displayName: 'Diego',
    username: 'diego123',
    verified: true,
    avatar: 'https://wallpaperaccess.com/full/2213424.jpg',
  },
  {
    text: 'Prueba en general',
    displayName: 'Diego',
    username: 'asdasd',
    verified: true,
    avatar:
      'https://qph.fs.quoracdn.net/main-qimg-20df28f3b31895e56cba6dbc0515c635',
  },
  {
    text: 'Primera prueba en general para el post',
    displayName: 'Diego',
    username: 'ignacio34',
    verified: true,
    avatar:
      'https://i.pinimg.com/564x/85/c0/87/85c087d76b4f0b058b2d1585e29a9ee7.jpg',
  },
];

function Feed() {
  const [posts, setPosts] = useState(initialState);

  return (
    <div className='feed'>
      <div className='feed__header'>
        <h2>General</h2>
      </div>

      <TweetBox setPosts={setPosts} />

      <FlipMove>
        {posts.map((post) => (
          <Post
            key={post.text}
            displayName={post.displayName}
            username={post.username}
            text={post.text}
            avatar={post.avatar}
            verified={post.verified}
          />
        ))}
      </FlipMove>
    </div>
  );
}

export default Feed;
