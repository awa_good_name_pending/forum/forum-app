import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { groupStartLoading } from '../../actions/groups';
import Feed from '../ui/Feed';
import Sidebar from '../ui/Sidebar';
import Widgets from '../ui/Widgets';
import { ForumModal } from './ForumModal';
import './Home.css';

const Home = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(groupStartLoading());
  }, [dispatch]);
  return (
    <div>
      <div className='app'>
        <Sidebar />
        <Feed />
        <Widgets />
      </div>
      <ForumModal />
    </div>
  );
};

export default Home;
